package org.gcube.portlets.user.wswidget.shared;

import org.gcube.vomanagement.usermanagement.model.GCubeUser;

public class AuthorizedUser {
	private GCubeUser user;
	private String token;
	private String context;
	public AuthorizedUser(GCubeUser user) {
		super();
		this.user = user;

	}
	public GCubeUser getUser() {
		return user;
	}
	public void setUser(GCubeUser user) {
		this.user = user;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AuthorizedUser [user=");
		builder.append(user);
		builder.append(", token=");
		builder.append(token);
		builder.append(", context=");
		builder.append(context);
		builder.append("]");
		return builder.toString();
	}	
}
