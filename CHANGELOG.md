
# Changelog for Workspace Widget Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.5.3]

- maven parent 1.2.0
- updated to shub 2

## [v1.5.2] - 2021-10-04

- Feature #22109 Workspace widget to load VRE Folders without using JCR indices

## [v1.5.1] - 2021-07-13

- Feature #21507 support new UMATokensProvider class
- Bug #21794 folder names were not URI encoded

## [v1.5.0] - 2021-04-06

- Removed legacy auth dependency

## [v1.4.1] - 2021-01-26

- Fix Bug #20552 Workspace widget item counter no longer works

## [v1.3.1] - 2019-10-09

- Updated to new storage hub api

## [v1.3.0] - 2019-03-04

- Updated to new storage hub exception handling<

- Feature #12796 Workspace widget: recent documents has a different behaviour on URL items

## [v1.0.0] - 2018-05-17

First release
